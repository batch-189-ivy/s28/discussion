//CRUD Operations

//Insert Documents (Create)
/*
	Syntax:
		Insert One Document
			-db.collectionNamme.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB",

			});


		Insert Many Documents - may clear brackets
			-db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				},

				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				},
			])

*/


db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});


db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},

		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "none"
		}

	])


//Miniactivity
db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},

		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},

		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		}

	])


//Find Documents (Read)
/*
	Syntax:
		-db.collectionName.find() - this will retrieve all our documents 
		-db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria
		-db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that match the criteria
		--db.collectionName.findOne({}) - will return the first document in our collection

*/


db.users.find({
	"firstName": "Jane"
})

db.users.findOne({
	"department": "none"
})

db.users.find({
	"lastName": "Armstrong",
	"age": 82
})


//Updating documents(Update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},

		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		})

		db.collectionName.updateMany(
			{
				"criteria": "value"
			},
			{
				$set: {
					"fieldToBeUpdated": "updatedValue"
				} 
			}
		)
*/


db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@gmail.com",
	"department": "none"
})

//Updating One Document

db.users.updateOne(
		{
			"firstName": "Test"
		},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"email": "billgates@mail.com",
				"department": "Operations",
				"status": "active"
			}
		}


	)


//Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
	}


)


//Updating Multiple Documents
db.users.updateMany(
		{
			"department": "none"
		},

		{
			$set: {
				"department": "HR"
			}
		}

	)


db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			$set: {
				"firstName": "Billyboy",
			}
		}


	)

//Renaming a field
db.users.updateMany(
		{},
		{
			$rename: {
				"department": "dept"
			}
		}
	)

//Mini activity


db.courses.updateOne(
		{
			"name": "HTML 101"
		},

		{
			$set: {
				"isActive": false
			}
		}
	)

db.courses.updateMany(
		{},
		{
			$set: {
				"Enrollees": 10
			}
		}

	)

db.users.insertOne({
	"firstName": "Test"
})

//Deleting a single document
/*
	Syntax:
		-db.collectionName.deleteOne({"criteria": "value"})
*/


db.users.deleteOne({
	"firstName": "Test"
})


//Deleting multiple documents
/*
	Syntax:
		-db.collectionName.deleteMany({"criteria": "value"})

*/


db.users.deleteMany({
	"dept": "HR"
})


